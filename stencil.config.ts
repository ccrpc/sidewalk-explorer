export const config = {
  namespace: 'sidewalk-explorer',
  outputTargets: [
    {
      type: 'www',
      baseUrl: '/sidewalk-explorer',
      serviceWorker: null
    }
  ],
  plugins: [],
  globalScript: 'src/global/sidewalk.ts',
  globalStyle: 'src/global/sidewalk.css'
};
