import { Component, Element, Listen, Prop, Watch, h } from "@stencil/core";
import bbox from "@turf/bbox";
import { getStateByNames, doOnce } from "../utils";

@Component({
  tag: "se-root",
  styleUrl: "root.css",
})
export class Root {
  ignoreClick: boolean = false;
  mapEl?: HTMLGlMapElement;
  styleEl?: HTMLGlStyleElement;

  @Element() el: HTMLSeRootElement;

  @Prop() baseUrl: string;
  @Prop() featureType: string;
  @Prop() field: string;
  @Prop() region: string;
  @Prop() municipality: string;
  @Prop({ mutable: true }) selectedFeature: any;
  @Prop() year: string;

  componentDidLoad() {
    this.updateFields();
    this.updateCounts();
    this.mapEl.map.on("click", () => {
      if (!this.ignoreClick) this.deselectFeature();
    });
    doOnce("se.intro", () => this.showIntro());
  }

  @Listen("glFeatureClick")
  handleClick(e: CustomEvent) {
    let feature = e.detail.features[0];
    if (!feature) return;
    if (feature.sourceLayer == "aggregated") {
      // We know that the feature will be two dimensional so we can use the 4 number version of BBox
      this.mapEl.map.fitBounds(
        bbox(feature) as [number, number, number, number]
      );
    } else {
      this.selectedFeature = feature;
    }
    this.ignoreClick = true;
    window.setTimeout(() => (this.ignoreClick = false), 100);
  }

  @Watch("featureType")
  @Watch("region")
  @Watch("municipality")
  @Watch("year")
  deselectFeature() {
    this.selectedFeature = null;
  }

  @Watch("featureType")
  @Watch("field")
  @Watch("selectedFeature")
  updateFields() {
    Array.from(this.el.querySelectorAll("se-feature-type")).forEach((ft) => {
      Array.from(ft.querySelectorAll("se-field")).forEach((field) => {
        field.visible =
          !this.selectedFeature &&
          ft.name === this.featureType &&
          field.name === this.field;
      });
    });
  }

  @Watch("region")
  @Watch("year")
  async updateCounts() {
    let state = getStateByNames(
      this.featureType,
      this.field,
      this.region,
      this.municipality,
      this.year
    );
    let region = state.region;
    let year = state.year;

    this.assignCounts("current", region.name, year.name);
    if (region.baselineYear != year.name) {
      this.assignCounts("baseline", region.name, region.baselineYear);
    } else {
      this.unassignCounts("baseline");
    }
  }
  // Loading the data -- by region or by municipality, this will assign the totals from the json summary file
  async assignCounts(
    prop: "current" | "baseline",
    region: string,
    year: string
  ) {
    let res = await fetch(`assets/summaries/${region}-${year}.json`);
    let data = await res.json();
    // Here data are actually being put into the array
    Array.from(this.el.querySelectorAll("se-feature-type")).forEach((ft) => {
      Array.from(ft.querySelectorAll("se-field")).forEach(
        (field) => (field[prop] = data[ft.value][field.value])
      );
    });
  }
  // Handling removal
  async unassignCounts(prop: "current" | "baseline") {
    Array.from(this.el.querySelectorAll("se-field")).forEach(
      (field) => (field[prop] = null)
    );
  }

  @Watch("municipality")
  @Watch("year")
  async updateMuniCounts() {
    let state = getStateByNames(
      this.featureType,
      this.field,
      this.region,
      this.municipality,
      this.year
    );
    let municipality = state.municipality;
    let year = state.year;

    this.assignCounts("current", municipality.name, year.name);
    if (municipality.baselineYear != year.name) {
      this.assignCounts(
        "baseline",
        municipality.name,
        municipality.baselineYear
      );
    } else {
      this.unassignCounts("baseline");
    }
  }
  // Loading the data -- by region or by municipality, this will assign the totals from the json summary file
  async assignMuniCounts(
    prop: "current" | "baseline",
    municipality: string,
    year: string
  ) {
    let res = await fetch(`assets/summaries/${municipality}-${year}.json`);
    let data = await res.json();
    // Here data are actually being put into the array
    Array.from(this.el.querySelectorAll("se-feature-type")).forEach((ft) => {
      Array.from(ft.querySelectorAll("se-field")).forEach(
        (field) => (field[prop] = data[ft.value][field.value])
      );
    });
  }

  // Handling removal
  async unassignMuniCounts(prop: "current" | "baseline") {
    Array.from(this.el.querySelectorAll("se-field")).forEach(
      (field) => (field[prop] = null)
    );
  }

  getFeatureDetails() {
    if (!this.selectedFeature) return;
    return (
      <se-feature-detail feature={this.selectedFeature}></se-feature-detail>
    );
  }

  showIntro() {
    this.el.querySelector("se-about-button").openModal(null);
  }

  render() {
    let state = getStateByNames(
      this.featureType,
      this.field,
      this.region,
      this.municipality,
      this.year
    );
    let imageUrl: string = null;
    if (this.selectedFeature) {
      let image = this.selectedFeature.properties.image;
      if (image) imageUrl = state.featureType.imageUrl + image;
    } else {
      imageUrl =
        `assets/images/` + `${state.featureType.name}-${state.field.name}.jpg`;
    }
    let secondary = this.selectedFeature
      ? "Feature Details"
      : state.field.label;

    return (
      <gl-app label="Sidewalk Explorer" menu={false} splitPane={false}>
        <se-settings-button slot="end-buttons"></se-settings-button>
        <gl-basemaps slot="end-buttons"></gl-basemaps>
        <gl-fullscreen slot="end-buttons"></gl-fullscreen>
        <se-about-button slot="end-buttons"></se-about-button>
        <div class="panes">
          <gl-map
            class="map-pane"
            longitude={-88.228878}
            latitude={40.110319}
            zoom={12}
            maxzoom={22}
            ref={(r: HTMLGlMapElement) => (this.mapEl = r)}
          >
            <se-style state={state}></se-style>
            <gl-style
              url="https://maps.ccrpc.org/basemaps/basic/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/basemaps/basic/preview.jpg"
              name="Basic"
              enabled={true}
            ></gl-style>
            <gl-style
              url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/basemaps/hybrid/preview.jpg"
              // just the thumbnail - doesn't need to be updated yet until we create a new preview
              name="2023 Aerial Imagery"
              enabled={false}
            ></gl-style>
          </gl-map>
          <div class="info-pane">
            <se-graphic-header
              hasSelection={this.selectedFeature}
              primary={state.featureType.label}
              secondary={secondary}
              image={imageUrl}
            ></se-graphic-header>
            <div class="gl-menu-content">
              <slot name="menu" />
              {this.getFeatureDetails()}
            </div>
          </div>
        </div>
      </gl-app>
    );
  }
}
