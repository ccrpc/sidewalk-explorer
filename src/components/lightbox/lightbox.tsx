import { Component, Element, Prop, h } from "@stencil/core";
import { findOrCreate } from "../utils";

@Component({
  styleUrl: "lightbox.css",
  tag: "se-lightbox",
})
export class Lightbox {
  @Element() el: HTMLElement;

  @Prop()
  modal: HTMLIonModalElement;

  @Prop() image: string;

  async closeModal() {
    if (this.modal == undefined) {
      this.modal = await findOrCreate("ion-modal");
    }
    this.modal.dismiss();
  }

  hostData() {
    return {
      style: {
        backgroundImage: this.image ? `url('${this.image}')` : "none",
      },
    };
  }

  render() {
    return [
      <ion-header translucent={true}>
        <ion-toolbar>
          <ion-title>Feature Image</ion-title>
          <ion-buttons slot="end">
            <ion-button onClick={() => this.closeModal()}>
              <ion-icon slot="start" name="checkmark"></ion-icon>
              Close
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
    ];
  }
}
